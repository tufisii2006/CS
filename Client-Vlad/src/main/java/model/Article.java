package model;

import java.io.Serializable;



public class Article implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1873466050570733461L;

	private int id;

	private String title;
	
	private String abstr;
	
	private String body;
	
	private Writer writer;
	public Article() {
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAbstr() {
		return abstr;
	}
	public void setAbstr(String abstr) {
		this.abstr = abstr;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Writer getWriter() {
		return writer;
	}
	public void setWriter(Writer writer) {
		this.writer = writer;
	}
	public int getId() {
		return id;
	}
	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", abstr=" + abstr + ", body=" + body + ", writer=" + writer
				+ "]";
	}
	
}
