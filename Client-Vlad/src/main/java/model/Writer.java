package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


public class Writer implements Serializable {

    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Writer [id=" + id + ", name=" + name + ", username=" + username + ", password=" + password
				+ ", listOfArticles=" + listOfArticles + "]";
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -3618464479154276617L;

	private int id;
	
	private String name;
	
	private String username;
	
	private String password;
	
	private List<Article> listOfArticles = new LinkedList<Article>();
	public Writer() {
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<Article> getListOfArticles() {
		return listOfArticles;
	}
	public void setListOfArticles(List<Article> listOfArticles) {
		this.listOfArticles = listOfArticles;
	}
	public int getId() {
		return id;
	}
}
