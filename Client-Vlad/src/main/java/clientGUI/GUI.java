package clientGUI;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Article;
import model.Writer;

import javax.sound.sampled.ReverbType;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class GUI {

	private JFrame frame;
	private JButton btnCiteste;
	private JTextField textField;
	private JLabel lblTitlu;
	private JTextField textField_user;
	private JTextField textField_pass;

	public GUI() {

		initialize();
		frame.setVisible(true);
	}

	 public List<Object> processInformation(List<Object> toSend) throws UnknownHostException, IOException {
	        Socket socket = new Socket("localhost", 8080);
	        ObjectOutputStream infoToGiveToServer = new ObjectOutputStream(socket.getOutputStream());
	        infoToGiveToServer.writeObject(toSend);
	        infoToGiveToServer.flush();

	        // Here we read the details from server
	        ObjectInputStream serverResponse = new ObjectInputStream(socket.getInputStream());	    
	        List<Object> list = null;
			try {
				list = (List<Object>) serverResponse.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        serverResponse.close();
	        infoToGiveToServer.close();
	        socket.close();
	        return list;
	    
	    }


	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 774, 431);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		btnCiteste = new JButton("Baga articol");
		btnCiteste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					List<Object> myList=new ArrayList<Object>();
					Article a=new Article();
					a.setTitle(textField.getText());
					String baga="Read";
					myList.add(baga);
					myList.add(a);
					processInformation(myList);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});
		btnCiteste.setBounds(75, 240, 114, 44);
		frame.getContentPane().add(btnCiteste);
		
		textField = new JTextField();
		textField.setBounds(89, 198, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		lblTitlu = new JLabel("Titlu");
		lblTitlu.setBounds(30, 198, 46, 14);
		frame.getContentPane().add(lblTitlu);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLogin.setBounds(390, 111, 72, 35);
		frame.getContentPane().add(lblLogin);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(318, 157, 86, 20);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(318, 201, 62, 17);
		frame.getContentPane().add(lblPassword);
		
		textField_user = new JTextField();
		textField_user.setBounds(390, 157, 86, 20);
		frame.getContentPane().add(textField_user);
		textField_user.setColumns(10);
		
		textField_pass = new JTextField();
		textField_pass.setBounds(390, 198, 86, 20);
		frame.getContentPane().add(textField_pass);
		textField_pass.setColumns(10);
		
		JButton btnLogin = new JButton("Log-In");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String username=new String();
				String pass=new String();
				username=textField_user.getText();
				pass=textField_pass.getText();
				try {
					List<Object> toSend=new ArrayList<Object>();
					String comanda="Writter";
					toSend.add(comanda);
					toSend.add(username);
					toSend.add(pass);
					List<Object> receive=new ArrayList<Object>();
					receive=processInformation(toSend);				
					for(Object o:receive) {
						if(o instanceof Writer && ((Writer) o).getUsername().equals(username)) {
							Writer wr=(Writer) o;
							new WritterGUI(wr);
						}
					}

					
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}
		});
		btnLogin.setBounds(371, 240, 105, 35);
		frame.getContentPane().add(btnLogin);
		
		JLabel lblImAReader = new JLabel("Im a reader");
		lblImAReader.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		lblImAReader.setBounds(600, 154, 100, 26);
		frame.getContentPane().add(lblImAReader);
		
		JButton btnReadArticles = new JButton("Read Articles");
		btnReadArticles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					List<Object> toSend=new ArrayList<Object>();
					String comanda="Reader";
					toSend.add(comanda);			
					List<Object> receive=new ArrayList<Object>();
					receive=processInformation(toSend);
					new ArticoleReader(receive);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnReadArticles.setBounds(560, 197, 140, 35);
		frame.getContentPane().add(btnReadArticles);
	}
}
