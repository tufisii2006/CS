package clientGUI;


import javax.swing.JFrame;

import model.*;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.awt.event.ActionEvent;

public class WritterGUI extends Observable {

	private JFrame frame;
	private Writer sesionWriter;
	private JTextField textField_titlu;
	private JTextField textField_abstract;
	private JTextField textField_body;
	private ArrayList<Observer> listObservers = new ArrayList<Observer>();
	ArticoleReader x ;
	
	public WritterGUI(Writer sesionWriter) {
		listObservers.add(x);
		this.sesionWriter=sesionWriter;
		initialize();
		frame.setVisible(true);
	}

	private void initialize() {
		this.addObserver(x);
		frame = new JFrame();
		frame.setBounds(100, 100, 798, 483);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblWellcome = new JLabel("WELLCOME:");
		lblWellcome.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		lblWellcome.setBounds(187, 22, 107, 35);
		frame.getContentPane().add(lblWellcome);
		
		JLabel lblNewLabel = new JLabel(sesionWriter.getName());
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(304, 25, 107, 30);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNumeArticol = new JLabel("Titlu");
		lblNumeArticol.setBounds(33, 102, 127, 35);
		frame.getContentPane().add(lblNumeArticol);
		
		JLabel lblAbstract = new JLabel("Abstract");
		lblAbstract.setBounds(33, 148, 46, 27);
		frame.getContentPane().add(lblAbstract);
		
		JLabel lblBody = new JLabel("Body");
		lblBody.setBounds(33, 184, 46, 23);
		frame.getContentPane().add(lblBody);
		
		textField_titlu = new JTextField();
		textField_titlu.setBounds(74, 109, 86, 20);
		frame.getContentPane().add(textField_titlu);
		textField_titlu.setColumns(10);
		
		textField_abstract = new JTextField();
		textField_abstract.setBounds(89, 151, 86, 20);
		frame.getContentPane().add(textField_abstract);
		textField_abstract.setColumns(10);
		
		textField_body = new JTextField();
		textField_body.setBounds(89, 185, 224, 185);
		frame.getContentPane().add(textField_body);
		textField_body.setColumns(10);
		
		JButton btnAdaugaArticol = new JButton("Adauga articol");
		btnAdaugaArticol.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Article art=new Article();
				art.setTitle(textField_titlu.getText());
				art.setBody(textField_body.getText());
				art.setWriter(sesionWriter);
				art.setAbstr(textField_abstract.getText());
				 List<Object> list = new ArrayList<Object>();
				 list.add("Adauga");
				 list.add(art);
			
				 System.out.println("Am bagat un articol nou");
				try {

					processInformation(list);
					setChanged();
					 notifyObservers();
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAdaugaArticol.setBounds(74, 397, 158, 36);
		frame.getContentPane().add(btnAdaugaArticol);
	}
	
	
	public List<Object> processInformation(List<Object> toSend) throws UnknownHostException, IOException {
        Socket socket = new Socket("localhost", 8080);
        ObjectOutputStream infoToGiveToServer = new ObjectOutputStream(socket.getOutputStream());
        infoToGiveToServer.writeObject(toSend);
        infoToGiveToServer.flush();

        // Here we read the details from server
        ObjectInputStream serverResponse = new ObjectInputStream(socket.getInputStream());	    
        List<Object> list = null;
		try {
			list = (List<Object>) serverResponse.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        serverResponse.close();
        infoToGiveToServer.close();
        socket.close();
        return list;
    
    }
	
	@Override
	public void addObserver(Observer o) {
		listObservers.add(o);
	}

	public void removeObserver(Observer o) {
		listObservers.remove(o);		
	}
	public void notifyObserver() {		
		for (Observer reader :listObservers) {
			reader.update(null, null);
		}
	}
}
