package clientGUI;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import model.*;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class ArticoleReader implements Observer {

	private JFrame frame;
	private List<Object> list;
	public JComboBox comboBox;
	private Observable observable;

	/**
	 * @return the allArticles
	 */
	public List<Article> getAllArticles() {
		return allArticles;
	}

	/**
	 * @param allArticles the allArticles to set
	 */
	public void setAllArticles(List<Article> allArticles) {
		this.allArticles = allArticles;
	}

	private List<Article> allArticles = new ArrayList<Article>();

	public ArticoleReader(List<Object> list) throws IOException {
		this.list = list;
		frame = new JFrame();
		initialize();
		frame.setVisible(true);
	}
	 private void setObservable(Observable observable) {
	        this.observable = observable;
	        this.observable.addObserver(this);  
	    }


	private void initialize() throws IOException {
		
		String[] titluri = new String[list.size()];
		
		int j = -0;
		for (Object a : list) {
			Article aux = (Article) a;
			allArticles.add(aux);
		}

		int i = 0;
		for (Article a : allArticles) {
			titluri[i] = a.getTitle();
			i++;
		}

		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(titluri));

		frame = new JFrame();
		frame.setBounds(100, 100, 877, 468);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(473, 70, 367, 335);
		frame.getContentPane().add(scrollPane);
		
				final JTextPane textPane = new JTextPane();
				scrollPane.setViewportView(textPane);

		comboBox.setBounds(51, 73, 208, 22);
		frame.getContentPane().add(comboBox);

		JLabel lblChoseArticleTo = new JLabel("Chose Article to read");
		lblChoseArticleTo.setBounds(70, 31, 189, 28);
		frame.getContentPane().add(lblChoseArticleTo);
		

		final JLabel labelTitluArticol = new JLabel("");
		labelTitluArticol.setBounds(573, 11, 96, 22);
		frame.getContentPane().add(labelTitluArticol);
		
		final JLabel labelAbstract = new JLabel("");
		labelAbstract.setBounds(561, 37, 108, 22);
		frame.getContentPane().add(labelAbstract);

		JButton btnRead = new JButton("Read");
		btnRead.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (Article a : allArticles) {
					if (comboBox.getSelectedItem().toString().equals(a.getTitle())) {
						labelAbstract.setText(a.getAbstr());
						labelTitluArticol.setText(a.getTitle());
						textPane.setText(a.getBody());
						
						update(null, null);
					}
				}

			}
		});
		btnRead.setBounds(91, 118, 108, 33);
		frame.getContentPane().add(btnRead);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblTitle.setBounds(473, 11, 46, 14);
		frame.getContentPane().add(lblTitle);
		
		JLabel lblAbstract = new JLabel("Abstract");
		lblAbstract.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblAbstract.setBounds(467, 45, 66, 14);
		frame.getContentPane().add(lblAbstract);
		
		
	}
	
	public List<Object> processInformation(List<Object> toSend) throws UnknownHostException, IOException {
        Socket socket = new Socket("localhost", 8080);
        ObjectOutputStream infoToGiveToServer = new ObjectOutputStream(socket.getOutputStream());
        infoToGiveToServer.writeObject(toSend);
        infoToGiveToServer.flush();

        // Here we read the details from server
        ObjectInputStream serverResponse = new ObjectInputStream(socket.getInputStream());	    
        List<Object> list = null;
		try {
			list = (List<Object>) serverResponse.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        serverResponse.close();
        infoToGiveToServer.close();
        socket.close();
        return list;
    
    }

	@Override
	public void update(Observable o, Object arg) {
		WritterGUI wgui;
		wgui = (WritterGUI) o;
		List<Object> toSend=new ArrayList<Object>();
		String comanda="Reader";
		toSend.add(comanda);
		List<Object> newList=new ArrayList<Object>();
		try {
			newList=processInformation(toSend);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] titluri = new String[newList.size()];
		final List<Article> allArticles = new ArrayList<Article>();
		int j = -0;
		for (Object a : newList) {
			Article aux = (Article) a;
			allArticles.add(aux);
		}

		int i = 0;
		for (Article a : allArticles) {
			titluri[i] = a.getTitle();
			i++;
		}
		this.allArticles=allArticles;
		comboBox.setModel(new DefaultComboBoxModel<String>(titluri));
	}
}
