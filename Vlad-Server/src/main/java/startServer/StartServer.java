package startServer;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;

import model.Article;
import model.Writer;
import repository.ArticleRepo;
import repository.WriterRepo;

public class StartServer {
	
	public static List<Socket> allClientsList= new ArrayList<Socket>();
	@SuppressWarnings("unchecked")
	public static void main(String[] argv) throws Exception {

		@SuppressWarnings("resource")
		ServerSocket serverSocket = new ServerSocket(8080);
		System.out.println("Server started");

		/*
		 * EntityManagerFactory entityManagerFactory = Persistence
		 * .createEntityManagerFactory("org.hibernate.tutorial.jpa"); EntityManager
		 * eniEntityManager = entityManagerFactory.createEntityManager();
		 */

		while (true) {

			List<Object> receive = new ArrayList<Object>();
			// Wait for client to connect
			Socket aNewClientSocket = serverSocket.accept();
			allClientsList.add(aNewClientSocket);
			
			System.out.println("Server-Client connexion established!!");

			ObjectInputStream toReceiveFromClient = new ObjectInputStream(aNewClientSocket.getInputStream());
			receive = (List<Object>) toReceiveFromClient.readObject();

			System.out.println(aNewClientSocket.getLocalPort());
			System.out.println(aNewClientSocket.getInetAddress());
			ArticleRepo repo = new ArticleRepo();
			WriterRepo repoWriter=new WriterRepo();

			switch ((String) receive.get(0)) {
			
			case "Read":
				List<Object> toSend = new ArrayList<Object>();
				Article a = (Article) receive.get(1);

				repo.insertArticle(a);
				toSend.add(a);
				ObjectOutputStream objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			case "Reader":

				List<Object> send = new ArrayList<Object>();
				List<Article> allArticle = new ArrayList<Article>();
				allArticle=repo.viewAllArticles();
				for (Article articl : allArticle) {
					send.add(articl);
				}
				ObjectOutputStream backToClient = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				backToClient.writeObject(send);
				backToClient.close();				
				toReceiveFromClient.close();
				aNewClientSocket.close();
				break;
				
			case "Writter":
				List<Object> sendList = new ArrayList<Object>();
				List<Writer> allWriters = new ArrayList<Writer>();
				allWriters=repoWriter.viewAllWriters();
				for(Writer w: allWriters) {
					sendList.add(w);
				}
				System.out.println(allWriters.size());
				ObjectOutputStream response = new ObjectOutputStream(aNewClientSocket.getOutputStream());
/*				Writer sesionWriter= repoWriter.writerGivenUsername((String)receive.get(1));
				JOptionPane.showMessageDialog(null, sesionWriter.getName());*/
				//sendList.add(sesionWriter);		
				
				response.writeObject(sendList);

				break;
			case "Adauga":
				List<Object> toDo = new ArrayList<Object>();
				toDo.add("Da");
				
				ObjectOutputStream ras = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				Article artRec=(Article) receive.get(1);
				repo.insertArticle(artRec);
				ras.writeObject(toDo);

				break;
			default:
				JOptionPane.showMessageDialog(null, "Invalid command");
				break;
			}

		}

	}
}
